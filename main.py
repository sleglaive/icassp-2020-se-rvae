#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code implements the speech enhancement method published in: S. Leglaive, 
X. Alameda-Pineda, L. Girin, R. Horaud, "A recurrent variational autoencoder 
for speech enhancement", IEEE International Conference on Acoustics Speech and 
Signal Processing (ICASSP), Barcelona, Spain, 2020.

Copyright (c) 2019-2020 by Inria and CentraleSupelec
Authored by Simon Leglaive (simon.leglaive@centralesupelec.fr)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License (version 3) 
as published by the Free Software Foundation.

You should have received a copy of the GNU Affero General Public License
along with this program (LICENSE.txt). If not, see 
<https://www.gnu.org/licenses/>.
"""

my_seed = 0
import numpy as np
np.random.seed(my_seed)
import torch
torch.manual_seed(my_seed)
import soundfile as sf 
import librosa
import sys
sys.path.append('./training')
from VAEs import RVAE, VAE
import os
from SE_algorithms import VEM, PEEM, MCEM

#%%#######################################################################
# Choosing the deep generative speech model
##########################################################################
# - 'FFNN': FFNN generative speech model
# - 'RNNenc-RNNdec': RNN generative speech model
# - 'BRNNenc-BRNNdec': BRNN generative speech model
##########################################################################

#vae_type = 'FFNN' 
#vae_type = 'RNNenc-RNNdec' 
vae_type = 'BRNNenc-BRNNdec'

#%%#######################################################################
# Choosing the speech enhancement algorithm
##########################################################################
# - 'mcem': Monte Carlo EM (only valid for FFNN generative speech model)
# - 'vem': Variational EM with fine-tune encoder (valid for all models)
# - 'peem': Point-estimate EM (valid for all models)
##########################################################################

#algo_type = 'mcem'
algo_type = 'vem'
#algo_type = 'peem'


#%% Parameters
    
data_dir = './audio'
mix_file = os.path.join(data_dir, 'mix_thierry_roland.wav') # mixture wavfile
output_dir = data_dir # path for saving the estimated speech and noise signals

fs = int(16e3) # Sampling rate
eps = np.finfo(float).eps # machine epsilon

wlen_sec = 64e-3 # STFT window length in seconds
hop_percent = 0.25  # hop size as a percentage of the window length
zp_percent = 0
wlen = wlen_sec*fs # window length of 64 ms
wlen = np.int(np.power(2, np.ceil(np.log2(wlen)))) # next power of 2
hop = np.int(hop_percent*wlen) # hop size
nfft = wlen + zp_percent*wlen # number of points of the DFT
win = np.sin(np.arange(.5,wlen-.5+1)/wlen*np.pi); # sine analysis window
    
device = 'cpu'
niter = 200 # results reported in the paper were obtained with 500 iterations 
nmf_rank = 8

#%% Load mixture and compute STFT

x, fs_x = sf.read(mix_file) 
x = x/np.max(x)
    
T_orig = len(x)
x_pad = librosa.util.fix_length(x, T_orig + wlen // 2)
X = librosa.stft(x_pad, n_fft=nfft, hop_length=hop, win_length=wlen, 
                 window=win)
F, N = X.shape
X_abs_2 = np.abs(X)**2

#%% Load VAE

if vae_type == 'FFNN':
    
    saved_model = ('./saved_model/WSJ0_2019-07-15-10h21_FFNN_VAE_latent_dim=16'
                   '/final_model_RVAE_epoch65.pt')
    
    input_dim = 513
    latent_dim = 16   
    hidden_dim_encoder = [128]
    activation = torch.tanh
    
    vae = VAE(input_dim=input_dim, latent_dim=latent_dim, 
                hidden_dim_encoder=hidden_dim_encoder,
                activation=activation)
    
elif vae_type == 'RNNenc-RNNdec':
    
    saved_model = ('./saved_model/WSJ0_2019-07-15-10h14_RVAE_RNNenc_RNNdec_'
                   'latent_dim=16/final_model_RVAE_epoch121.pt')
    
    input_dim = 513
    latent_dim = 16   
    hidden_dim = 128
    num_LSTM = 1
    num_dense_enc = 1
    bidir_enc_s = False
    bidir_dec = False
    
    vae = RVAE(input_dim=input_dim, h_dim=hidden_dim, z_dim=latent_dim, 
               num_LSTM=num_LSTM, num_dense_enc=num_dense_enc, 
               bidir_enc_s=bidir_enc_s, bidir_dec=bidir_dec, device=device)     
    
elif vae_type == 'BRNNenc-BRNNdec':
    
    saved_model = ('./saved_model/WSJ0_2019-07-15-10h01_RVAE_BRNNenc_BRNNdec_'
                   'latent_dim=16/final_model_RVAE_epoch145.pt')

        
    input_dim = 513
    latent_dim = 16   
    hidden_dim = 128
    num_LSTM = 1
    num_dense_enc = 1
    bidir_enc_s = True
    bidir_dec = True
    
    vae = RVAE(input_dim=input_dim, h_dim=hidden_dim, z_dim=latent_dim, 
               num_LSTM=num_LSTM, num_dense_enc=num_dense_enc, 
               bidir_enc_s=bidir_enc_s, bidir_dec=bidir_dec, device=device)     
    
else:
    raise NameError('Unknown VAE type')

vae.load_state_dict(torch.load(saved_model, map_location=device))
vae.eval()

#%% Initialize noise model parameters

W_init = np.maximum(np.random.rand(F,nmf_rank), eps)
H_init = np.maximum(np.random.rand(nmf_rank, N), eps)

g_init = np.ones(N)

# Compute the first latent variable sample
with torch.no_grad():
    X_abs_2 = X_abs_2.T
    X_abs_2 = torch.from_numpy(X_abs_2.astype(np.float32))
    X_abs_2 = X_abs_2.to(device)
    Z_init, _, _ = vae.encode(X_abs_2) # corresponds to the encoder mean

Z_init = Z_init.numpy().T

#%% Define speech enhancement algorithm

if algo_type == 'mcem':
    
    print('MCEM')
    
    nsamples_E_step = 10
    nsamples_WF = 25
    burnin_E_step = 30
    burnin_WF = 75
    var_RW = 0.01
    
    algo = MCEM(X=X, W=W_init, H=H_init, g=g_init, Z=Z_init, vae=vae, 
                device=device, niter=niter, nsamples_E_step=nsamples_E_step, 
                burnin_E_step=burnin_E_step, nsamples_WF=nsamples_WF, 
                burnin_WF=burnin_WF, var_RW=var_RW)
    
elif algo_type == 'vem':
    
    print('VEM')
    
    nsamples_E_step = 1
    nsamples_WF = 1
    lr = 1e-2
    
    if vae_type == 'FFNN':
        nepochs_E_step = 10 # IMPORTANT PARAMETER: If 1 epoch, bad results 
    else:
        nepochs_E_step = 1
    
    algo = VEM(X=X, W=W_init, H=H_init, g=g_init, vae=vae, device=device,
               niter=niter, nsamples_E_step=nsamples_E_step,
               nsamples_WF=nsamples_WF, lr=lr, 
               nepochs_E_step=nepochs_E_step)

elif algo_type == 'peem':
    
    print('PEEM')
    
    lr = 1e-2
    
    if vae_type == 'FFNN':
        nepochs_E_step = 10 # IMPORTANT PARAMETER: If 1 epoch, bad results 
    else:
        nepochs_E_step = 1

    algo = PEEM(X=X, W=W_init, H=H_init, g=g_init, Z=Z_init, vae=vae, 
                device=device, niter=niter, lr=lr, 
                nepochs_E_step=nepochs_E_step)
    
else:

    raise NameError('Unknown algorithm')
    
    
#%% Run speech enhancement algorithm

cost = algo.run()

#%% Save estimated sources

path, mix_name = os.path.split(mix_file)
mix_name = mix_name[4:-4]

s_hat = librosa.istft(stft_matrix=algo.S_hat, hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)
n_hat = librosa.istft(stft_matrix=algo.N_hat, hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)

librosa.output.write_wav(os.path.join(output_dir, 'speech_est_' + mix_name + 
                                      '_' + vae_type + '_' + algo_type + 
                                      '.wav'), s_hat, fs)
librosa.output.write_wav(os.path.join(output_dir,'noise_est_' + mix_name + 
                                      '_' + vae_type + '_' + algo_type + 
                                      '.wav'), n_hat, fs)  
librosa.output.write_wav(os.path.join(output_dir,'mix_norm_' + mix_name + 
                                      '.wav'), x, fs)  
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code implements the speech enhancement method published in: S. Leglaive, 
X. Alameda-Pineda, L. Girin, R. Horaud, "A recurrent variational autoencoder 
for speech enhancement", IEEE International Conference on Acoustics Speech and 
Signal Processing (ICASSP), Barcelona, Spain, 2020.

Copyright (c) 2019-2020 by Inria and CentraleSupelec
Authored by Simon Leglaive (simon.leglaive@centralesupelec.fr)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License (version 3) 
as published by the Free Software Foundation.

You should have received a copy of the GNU Affero General Public License
along with this program (LICENSE.txt). If not, see 
<https://www.gnu.org/licenses/>.
"""


my_seed = 0
import numpy as np
np.random.seed(my_seed)
import torch
torch.manual_seed(my_seed)
from torch import optim
import time


class EM:
    """
    Meant to be an abstract class that should not be instantiated but only 
    inherited.
    """
    
    def __init__(self, X, W, H, g, vae, niter=100, device='cpu'):
        
        self.X = X # mixture STFT, shape (F,N)
        self.X_abs_2 = np.abs(X)**2 # mixture power spectrogram, shape (F, N)
        self.W = W # NMF dictionary matrix, shape (F, K)
        self.H = H # NMF activation matrix, shape (K, N)
        self.compute_Vb() # noise variance, shape (F, N)
        self.vae = vae # variational autoencoder 
        self.niter = niter # number of iterations
        self.g = g # gain parameters, shape (, N)
        self.Vs = None # speech variance, shape (R, F, N), where R corresponds 
        # to different draws of the latent variables fed as input to the vae
        # decoder
        self.Vs_scaled = None # speech variance multiplied by gain, 
        # shape (R, F, N)
        self.Vx = None # mixture variance, shape (R, F, N)
            
        self.device = device
    
    def np2tensor(self, x):
        y = torch.from_numpy(x.astype(np.float32))
        return y

    def tensor2np(self, x):
        y = x.numpy()
        return y
    
    def compute_expected_neg_log_like(self):
        return np.mean(np.log(self.Vx) + self.X_abs_2/self.Vx)
    
    def compute_Vs(self, Z):
        pass        
    
    def compute_Vs_scaled(self):
        self.Vs_scaled = self.g * self.Vs
    
    def compute_Vx(self):
        self.Vx = self.Vs_scaled + self.Vb
        
    def compute_Vb(self):
        self.Vb = self.W @ self.H
    
    def E_step(self):
        # The E-step aims to generate latent variables in order to compute the 
        # cost function required for the M-step. These samples are then also 
        # used to update the model parameters at the M-step.
        pass
    
    def M_step(self):
        # The M-step aims to update W, H and g
        
        if self.Vx.ndim == 2:
            # Vx and Vs are expected to be of shape (R, F, N) so we add a
            # singleton dimension.
            # This will happen for the PEEM algorithm only where there is no
            # sampling of the latent variables according to their posterior.
            rem_dim = True
            self.Vx = self.Vx[np.newaxis,:,:]
            self.Vs = self.Vs[np.newaxis,:,:]
            self.Vs_scaled = self.Vs_scaled[np.newaxis,:,:]
        else:
            rem_dim = False
        
        # update W
        num = (self.X_abs_2*np.sum(self.Vx**-2, axis=0)) @ self.H.T
        den = np.sum(self.Vx**-1, axis=0) @ self.H.T
        self.W = self.W*(num/den)**.5
        
        # update variances
        self.compute_Vb()
        self.compute_Vx()
            
        # update H
        num = self.W.T @ (self.X_abs_2*np.sum(self.Vx**-2, axis=0))
        den = self.W.T @ np.sum(self.Vx**-1, axis=0)
        self.H = self.H*(num/den)**.5
        
        # update variances
        self.compute_Vb()
        self.compute_Vx()
        
        # normalize W and H
        norm_col_W = np.sum(np.abs(self.W), axis=0)
        self.W = self.W/norm_col_W[np.newaxis,:]
        self.H = self.H*norm_col_W[:,np.newaxis]
        
        # Update g
        num = np.sum(self.X_abs_2*np.sum(self.Vs*(self.Vx**-2), axis=0), 
                     axis=0)
        den = np.sum(np.sum(self.Vs*(self.Vx**-1), axis=0), axis=0)
        self.g = self.g*(num/den)**.5

        # remove singleton dimension if necessary        
        if rem_dim:
            self.Vx = np.squeeze(self.Vx)
            self.Vs = np.squeeze(self.Vs)
            self.Vs_scaled = np.squeeze(self.Vs_scaled)
        
        # update variances
        self.compute_Vs_scaled()
        self.compute_Vx()
        
    
    def run(self):
        
        cost = np.zeros(self.niter)
        
        t0 = time.time()
        
        for n in np.arange(self.niter):
            
            self.E_step()
           
            self.M_step()
            
            cost[n] = self.compute_expected_neg_log_like() # this cost only
            # corresponds to the expectation of the negative log likelihood
            # taken w.r.t the posterior distribution of the latent variables.
            # It basically tells us if the model fits the observations.
            # We could also compute the full variational free energy.
            
            print("iter %d/%d - cost=%.4f" % (n+1, self.niter, cost[n]))
            
        elapsed = time.time() - t0
        
        WFs, WFn = self.compute_WF(sample=True)
            
        self.S_hat = WFs*self.X
        self.N_hat = WFn*self.X
        
        print("elapsed time: %.4f s" % (elapsed))
            
        return cost

#%%
        
class VEM(EM):
    
    def __init__(self, X, W, H, g, vae, device, niter=100, nsamples_E_step=1, 
                 nsamples_WF=1, lr=1e-2, nepochs_E_step=10, batch_size=0):
        
        super().__init__(X=X, W=W, H=H, g=g, vae=vae, niter=niter, 
             device=device)
        
        
        self.nsamples_E_step = nsamples_E_step
        self.nsamples_WF = nsamples_WF
        
        # mixture power spectrogram as tensor, shape (F, N)
        self.X_abs_2_t = self.np2tensor(self.X_abs_2).to(self.device) 
        
        # Define the encoder parameters as the ones to optimize during the 
        # E-step
        self.params_to_optimize = []
        if type(vae).__name__ == 'VAE':            
            for name in vae.named_parameters():
                if 'encoder' in name[0]:
                    self.params_to_optimize.append(name[1])
                elif 'latent' in name[0]:
                    self.params_to_optimize.append(name[1])
        elif type(vae).__name__ == 'RVAE':
            for name in vae.named_parameters():
                if 'enc_' in name[0]:
                    self.params_to_optimize.append(name[1])
        else:
            raise NameError('Unknown VAE type')
            
        self.optimizer = optim.Adam(self.params_to_optimize, lr=lr) # optimizer

        # VERY IMPORTANT PARAMETERS: If 1, bad results in the FFNN VAE case
        self.nepochs_E_step = nepochs_E_step 
    
        self.vae.train() # vae in train mode     
    
    def sample_posterior(self, nsamples=5):
        N = self.X.shape[1]
        
        if hasattr(self.vae, 'latent_dim'):
            L = self.vae.latent_dim
        elif hasattr(self.vae, 'z_dim'):
            L = self.vae.z_dim
            
        Z_t = torch.zeros((N, nsamples, L))
        with torch.no_grad():  
            for r in np.arange(nsamples):
                 _, _, Z_t[:,r,:] = self.vae.encode(torch.t(self.X_abs_2_t))
        return Z_t
    
    def compute_Vs(self, Z):
        """ Z: tensor of shape (N, R, L) """
        with torch.no_grad():  
            Vs_t = self.vae.decode(Z) # (N, R, F)
        
        if len(Vs_t.shape) == 2: 
            # shape is (N, F) but we need (N, R, F)
            Vs_t = Vs_t.unsqueeze(1) # add a dimension in axis 1
    
        self.Vs = np.moveaxis(self.tensor2np(Vs_t), 0, -1)           
        
    def E_step(self):
        """
        - update the parameters of q(z | x ), i.e. the parameters of the 
        encoder when the mixture spectrogram is fed as input
        - sample from q(z | x) 
        - compute Vs, Vs_scaled and Vx """
        
        # vector of gain parameters as tensor, shape (, N)
        g_t = self.np2tensor(self.g).to(self.device)
        
        # noise variances as tensor, shape (F, N)
        Vb_t = self.np2tensor(self.Vb).to(self.device)
        
        def closure():
            # reset gradients
            self.optimizer.zero_grad()
            
            # forward pass in the vae with mixture spectrogram as input
            Vs_t, mean_t, logvar_t, _ = self.vae(torch.t(self.X_abs_2_t))
            Vs_t = torch.t(Vs_t) # tensor of shape (F, N)
            mean_t = torch.t(mean_t) # tensor of shape (L, N)
            logvar_t = torch.t(logvar_t) # tensor of shape (L, N)
        
            Vx_t = g_t*Vs_t + Vb_t # likelihood variances, tensor of shape 
            # (F, N)
            
            # compute loss, do backward and update parameters
            loss = ( torch.sum(self.X_abs_2_t/Vx_t + torch.log(Vx_t)) - 
                    0.5*torch.sum(logvar_t - mean_t.pow(2) - logvar_t.exp()) )
            loss.backward()
            return loss
        
        for epoch in np.arange(self.nepochs_E_step):
            
            self.optimizer.step(closure)
        
        # sample from posterior
        Z_t = self.sample_posterior(self.nsamples_E_step) # tensor of shape 
        # (N, R, L)
        
        # compute variances
        self.compute_Vs(Z_t) 
        self.compute_Vs_scaled()
        self.compute_Vx()
    
    def compute_WF(self, sample=False):
        
        if sample:
            # sample from posterior
            Z_t = self.sample_posterior(self.nsamples_WF) # tensor of shape 
            # (N, R, L)
            
            # compute variances
            self.compute_Vs(Z_t) 
            self.compute_Vs_scaled()
            self.compute_Vx()
        
        # compute Wiener Filters
        WFs = np.mean(self.Vs_scaled/self.Vx, axis=0)
        WFn = np.mean(self.Vb/self.Vx, axis=0)
        
        return WFs, WFn
            
        
#%%
        
class PEEM(EM):
    
    def __init__(self, X, W, H, g, Z, vae, niter, device, lr=1e-2, 
                 nepochs_E_step=10):
        
        super().__init__(X=X, W=W, H=H, g=g, vae=vae, niter=niter, 
             device=device)
                
        # mixture power spectrogram as tensor, shape (F, N)
        self.X_abs_2_t = self.np2tensor(self.X_abs_2).to(self.device) 

        # intial value for the latent variables
        self.Z = Z # shape (L, N)
        self.Z_t = self.np2tensor(self.Z).to(self.device)
                
        # optimizer for the E-step     
        self.optimizer = optim.Adam([self.Z_t.requires_grad_()], lr=lr) 
        
        # VERY IMPORTANT PARAMETERS: If 1, bad results in the FFNN VAE case
        self.nepochs_E_step = nepochs_E_step 
        
        self.vae.eval() # vae in eval mode
        
    def compute_Vs(self, Z):
        """ Z: tensor of shape (N, L) """
        
        Vs_t = torch.t(self.vae.decode(torch.t(Z))) # (F, N)
    
        self.Vs = self.tensor2np(Vs_t.detach())
        
        return Vs_t
    
    def loss_function(self, V_x, X_abs_2_tensor, z):   
        
        neg_likelihood = torch.sum( torch.log(V_x) + X_abs_2_tensor / V_x )
        
        neg_prior = torch.sum(z.pow(2)) 
        
        return neg_likelihood + neg_prior
        
    def E_step(self):
        """ """
        # vector of gain parameters as tensor, shape (, N)
        g_t = self.np2tensor(self.g).to(self.device)
        
        # noise variances as tensor, shape (F, N)
        Vb_t = self.np2tensor(self.Vb).to(self.device)
        
        
        def closure():
            # reset gradients
            self.optimizer.zero_grad()
            # compute speech variance
            Vs_t = self.compute_Vs(self.Z_t)
            # compute likelihood variance
            Vx_t = g_t*Vs_t + Vb_t
            # compute loss, do backward and update latent variables
            loss = ( torch.sum(self.X_abs_2_t/Vx_t + torch.log(Vx_t)) + 
                    torch.sum(self.Z_t.pow(2)) )
            loss.backward()
            return loss
        
        # Some optimization algorithms such as Conjugate Gradient and LBFGS  
        # need to reevaluate the function multiple times, so we have to pass 
        # in a closure that allows them to recompute our model. The closure  
        # should clear the gradients, compute the loss, and return it.
        for epoch in np.arange(self.nepochs_E_step):
            self.optimizer.step(closure)  
        
        # update numpy array from the new tensor
        self.Z = self.tensor2np(self.Z_t.detach())
        
        # compute variances
        self.compute_Vs(self.Z_t)
        self.compute_Vs_scaled()
        self.compute_Vx()
        
    def compute_WF(self, sample=False):
        # sample parameter useless
        
        # compute Wiener Filters
        WFs = self.Vs_scaled/self.Vx
        WFn = self.Vb/self.Vx
        
        return WFs, WFn
         
#%% 
        
class MCEM(EM):
    
    def __init__(self, X, W, H, g, Z, vae, niter, device, nsamples_E_step=10, 
                 burnin_E_step=30, nsamples_WF=25, burnin_WF=75, var_RW=0.01):
        
        super().__init__(X=X, W=W, H=H, g=g, vae=vae, niter=niter, 
             device=device)

        if type(vae).__name__ == 'RVAE':
            raise NameError('MCEM algorithm only valid for FFNN VAE')
        
        self.Z = Z # Last draw of the latent variables, shape (L, N)
        self.nsamples_E_step = nsamples_E_step
        self.burnin_E_step = burnin_E_step
        self.nsamples_WF = nsamples_WF
        self.burnin_WF = burnin_WF
        self.var_RW = var_RW        
        
        # mixture power spectrogram as tensor, shape (F, N)
        self.X_abs_2_t = self.np2tensor(self.X_abs_2).to(self.device) 
        
        self.vae.eval() # vae in eval mode
        
        
        
    def sample_posterior(self, Z, nsamples=10, burnin=30):
        # Metropolis-Hastings
        
        F, N = self.X.shape
        
        if hasattr(self.vae, 'latent_dim'):
            L = self.vae.latent_dim
        elif hasattr(self.vae, 'z_dim'):
            L = self.vae.z_dim
        
        # random walk variance as tensor
        var_RM_t = torch.tensor(np.float32(self.var_RW))
        
        # latent variables sampled from the posterior
        Z_sampled_t = torch.zeros(N, nsamples, L)
        
        # intial latent variables as tensor, shape (L, N)        
        Z_t = self.np2tensor(Z).to(self.device)        
        # speech variances as tensor, shape (F, N)
        Vs_t = torch.t(self.vae.decode(torch.t(Z_t)))
        # vector of gain parameters as tensor, shape (, N)
        g_t = self.np2tensor(self.g).to(self.device)
        # noise variances as tensor, shape (F, N)
        Vb_t = self.np2tensor(self.Vb).to(self.device)
        # likelihood variances as tensor, shape (F, N)
        Vx_t = g_t*Vs_t + Vb_t
        
        cpt = 0
        averaged_acc_rate = 0
        for m in np.arange(nsamples+burnin):
            
            # random walk over latent variables
            Z_prime_t = Z_t + torch.sqrt(var_RM_t)*torch.randn(L, N)
            
            # compute associated speech variances
            Vs_prime_t = torch.t(self.vae.decode(torch.t(Z_prime_t))) # (F, N)
            Vs_prime_scaled_t = g_t*Vs_prime_t
            Vx_prime_t = Vs_prime_scaled_t + Vb_t
            
            # compute log of acceptance probability
            acc_prob = ( torch.sum(torch.log(Vx_t) - torch.log(Vx_prime_t) + 
                                   (1/Vx_t - 1/Vx_prime_t)*self.X_abs_2_t, 0) + 
                        .5*torch.sum( Z_t.pow(2) - Z_prime_t.pow(2), 0) )
            
            # accept/reject
            is_acc = torch.log(torch.rand(N)) < acc_prob
            
            averaged_acc_rate += ( torch.sum(is_acc).numpy()/
                                  np.prod(is_acc.shape)*100/(nsamples+burnin) )
            
            Z_t[:,is_acc] = Z_prime_t[:,is_acc]
            
            # update variances
            Vs_t = torch.t(self.vae.decode(torch.t(Z_t)))
            Vx_t = g_t*Vs_t + Vb_t
            
            if m > burnin - 1:
                Z_sampled_t[:,cpt,:] = torch.t(Z_t)
                cpt += 1
        
        print('averaged acceptance rate: %f' % (averaged_acc_rate))
        
        return Z_sampled_t        
        
            
    def compute_Vs(self, Z):
        """ Z: tensor of shape (N, R, L) """
        with torch.no_grad():  
            Vs_t = self.vae.decode(Z) # (N, R, F)
        
        if len(Vs_t.shape) == 2: 
            # shape is (N, F) but we need (N, R, F)
            Vs_t = Vs_t.unsqueeze(1) # add a dimension in axis 1
    
        self.Vs = np.moveaxis(self.tensor2np(Vs_t), 0, -1)  # (R, F, N)
        
        
    def E_step(self):
        """
        """

        # sample from posterior
        Z_t = self.sample_posterior(self.Z, self.nsamples_E_step, 
                                    self.burnin_E_step) # (N, R, L)
        
        # update last draw
        self.Z = self.tensor2np(torch.squeeze(Z_t[:,-1,:])).T
        
        # compute variances
        self.compute_Vs(Z_t) 
        self.compute_Vs_scaled()
        self.compute_Vx()
            
    def compute_WF(self, sample=False):
        
        if sample:
            # sample from posterior
            Z_t = self.sample_posterior(self.Z, self.nsamples_WF, 
                                        self.burnin_WF)
            
            # compute variances
            self.compute_Vs(Z_t) 
            self.compute_Vs_scaled()
            self.compute_Vx()
        
        WFs = np.mean(self.Vs_scaled/self.Vx, axis=0)
        WFn = np.mean(self.Vb/self.Vx, axis=0)
        
        return WFs, WFn
        
        
        
        
    
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code implements the speech enhancement method published in: S. Leglaive, 
X. Alameda-Pineda, L. Girin, R. Horaud, "A recurrent variational autoencoder 
for speech enhancement", IEEE International Conference on Acoustics Speech and 
Signal Processing (ICASSP), Barcelona, Spain, 2020.

Copyright (c) 2019-2020 by Inria and CentraleSupelec
Authored by Simon Leglaive (simon.leglaive@centralesupelec.fr)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License (version 3) 
as published by the Free Software Foundation.

You should have received a copy of the GNU Affero General Public License
along with this program (LICENSE.txt). If not, see 
<https://www.gnu.org/licenses/>.
"""


my_seed = 0
import numpy as np
np.random.seed(my_seed)
import torch
torch.manual_seed(my_seed)
from torch.utils import data
from torch import optim
from speech_dataset import SpeechDatasetSequences
import matplotlib
matplotlib.use('Agg')     
import matplotlib.pyplot as plt
import librosa
from VAEs import RVAE
import os
import socket
import pickle
import datetime

hostname =  socket.gethostname() 
print('HOSTNAME: ' + hostname)
date = datetime.datetime.now().strftime("%Y-%m-%d-%Hh%M")

dataset_name = 'WSJ0'
train_data_dir = '/data/datasets/clean_speech/wsj0_si_tr_s'
val_data_dir = '/data/datasets/clean_speech/wsj0_si_dt_05'
    
#%% network parameters

input_dim = 513
latent_dim = 16   
hidden_dim = 128
num_LSTM = 1
num_dense_enc = 1
bidir_enc_s = True
bidir_dec = True

#%% create directory for results
    
if bidir_enc_s:
    enc_type = 'BRNNenc_'
else:
    enc_type = 'RNNenc_'
    
if bidir_dec:
    dec_type = 'BRNNdec_'
else:
    dec_type = 'RNNdec_'    

    
res_folder_name = (dataset_name + '_' + date + '_RVAE_' + enc_type + dec_type 
                   + 'latent_dim=' + str(latent_dim))
        
save_dir = os.path.join('../saved_model', res_folder_name)

if not(os.path.isdir(save_dir)):
    os.makedirs(save_dir)
    
print(save_dir)

#%% STFT parameters

wlen_sec = 64e-3
hop_percent = 0.25
fs = 16000
zp_percent = 0
trim = True
verbose = False

#%% training parameters

device = 'cuda'
    
train_file_list = librosa.util.find_files(train_data_dir, ext='wav')
val_file_list = librosa.util.find_files(val_data_dir, ext='wav')

lr = 0.001
epochs = 500
batch_size = 32
sequence_len = 50
num_workers = 1 # if higher, may mess-up everything as the data loading is 
# based on some class attributes rather than on the 'index' variable 
# of __getitem__.
shuffle_file_list = True
shuffle_samples_in_batch = True

save_frequency = 10
early_stopping_patience = 20

# create dataloader 
train_dataset = SpeechDatasetSequences(file_list=train_file_list, 
                                       sequence_len=sequence_len, 
                                       wlen_sec=wlen_sec, 
                                       hop_percent=hop_percent, fs=fs, 
                                       zp_percent=zp_percent, trim=trim, 
                                       verbose=True, batch_size=batch_size, 
                                       shuffle_file_list=shuffle_file_list,
                                       name=dataset_name)

val_dataset = SpeechDatasetSequences(file_list=val_file_list, 
                                     sequence_len=sequence_len, 
                                     wlen_sec=wlen_sec, 
                                     hop_percent=hop_percent, fs=fs, 
                                     zp_percent=zp_percent, trim=trim, 
                                     verbose=verbose, batch_size=batch_size, 
                                     shuffle_file_list=shuffle_file_list,
                                     name=dataset_name)

# torch load will call __getitem__ of TIMIT to create Batch by randomly 
# (if shuffle=True) selecting data sample.
train_dataloader = data.DataLoader(train_dataset, batch_size=batch_size, 
                                   shuffle=shuffle_samples_in_batch, 
                                   num_workers=num_workers)

val_dataloader = data.DataLoader(val_dataset, batch_size=batch_size, 
                                   shuffle=shuffle_samples_in_batch, 
                                   num_workers=num_workers)

# init model
vae = RVAE(input_dim=input_dim, h_dim=hidden_dim, z_dim=latent_dim, 
           batch_size=batch_size, num_LSTM=num_LSTM, 
           num_dense_enc=num_dense_enc, bidir_enc_s=bidir_enc_s, 
           bidir_dec=bidir_dec, device=device).to(device)       


num_params = sum(p.numel() for p in vae.parameters() if p.requires_grad)

# optimizer
optimizer = optim.Adam(vae.parameters(), lr=lr)

# loss function
def loss_function(recon_x, x, mu, logvar):  
    recon = torch.sum(  x/recon_x - torch.log(x/recon_x) - 1 ) 
    KLD = -0.5 * torch.sum(logvar - mu.pow(2) - logvar.exp())
    return recon + KLD

#%% main loop for training 
    
train_loss = np.zeros((epochs,))
val_loss = np.zeros((epochs,))
best_val_loss = np.inf
cpt_patience = 0

for epoch in range(epochs):
    
    vae.train()
    
    for batch_idx, batch in enumerate(train_dataloader):
        batch = batch.permute(-1,0,1) # (sequence_len, batch_size, input_dim)
        batch = batch.to(device)
        optimizer.zero_grad()
        recon_batch, mean, logvar, z = vae(batch)
        loss = loss_function(recon_batch, batch, mean, logvar)
        loss.backward()
        train_loss[epoch] += loss.item()
        optimizer.step()    
        
        
        
    for batch_idx_val, batch in enumerate(val_dataloader):
        batch = batch.permute(-1,0,1) # (sequence_len, batch_size, input_dim)
        batch = batch.to(device)
        recon_batch, mu, logvar, z = vae(batch)
        loss = loss_function(recon_batch, batch, mu, logvar)
        val_loss[epoch] += loss.item()
        
    if val_loss[epoch] < best_val_loss:
        best_val_loss = val_loss[epoch]
        cpt_patience = 0
        best_state_dict = vae.state_dict()
        cur_best_epoch = epoch
    else:
        cpt_patience += 1
    
    train_loss[epoch] = train_loss[epoch] / train_dataset.num_samples
    val_loss[epoch] = val_loss[epoch] /  val_dataset.num_samples
    
    print('====> Epoch: {} train loss: {:.4f} val loss: {:.4f}'.format(
          epoch, train_loss[epoch], val_loss[epoch]))
    
    if cpt_patience == early_stopping_patience:
        print('Early stopping patience achieved')
        break
    
    if epoch % save_frequency == 0:
            # save parameters of your model
            save_file = os.path.join(save_dir, 'RVAE_epoch' + 
                                     str(cur_best_epoch) + '.pt')
            torch.save(best_state_dict, save_file)

train_loss = train_loss[:epoch+1]
val_loss = val_loss[:epoch+1]

save_file = os.path.join(save_dir, 'final_model_RVAE_epoch' + 
                         str(cur_best_epoch) + '.pt')
torch.save(best_state_dict, save_file)

loss_file = os.path.join(save_dir, 'loss_RVAE.pckl')

with open(loss_file, 'wb') as f:
        pickle.dump([train_loss, val_loss], f)

#%% Save parameters

dic_params = {'input_dim':input_dim,
              'latent_dim':latent_dim,
              'hidden_dim':hidden_dim,
              'num_LSTM':num_LSTM,
              'num_dense_enc':num_dense_enc,
              'bidir_enc_s':bidir_enc_s,
              'bidir_dec':bidir_dec,
              'hidden_dim':hidden_dim,
              'wlen_sec':wlen_sec,
              'hop_percent':hop_percent,
              'fs':fs,
              'zp_percent':zp_percent,
              'trim':trim,
              'epochs':epochs,
              'batch_size':batch_size,
              'sequence_len':sequence_len,
              'num_workers':num_workers,
              'shuffle_file_list':shuffle_file_list,
              'shuffle_samples_in_batch':shuffle_samples_in_batch,
              'save_frequency':save_frequency,
              'early_stopping_patience':early_stopping_patience,
              'my_seed':my_seed,
              'train_data_dir':train_data_dir,
              'val_data_dir':val_data_dir,
              'num_params':num_params,
              'date':date}

# Write the VAE and training parameters to a text file
params_text_file = os.path.join(save_dir, 'parameters.txt')
with open(params_text_file, 'w') as f:
        for key, value in dic_params.items():
            f.write('%s:%s\n' % (key, value))


# Write the VAE and training parameters to a pickle file
params_pckl_file = os.path.join(save_dir, 'parameters.pckl')
f = open(params_pckl_file, 'wb')
pickle.dump(dic_params, f)
f.close()


# save loss figure   
plt.clf()
plt.plot(train_loss, '--o')
plt.plot(val_loss, '--x')
plt.legend(('train loss', 'val loss'))
plt.xlabel('epochs')

loss_figure_file = os.path.join(save_dir, 'loss.pdf')

plt.savefig(loss_figure_file)
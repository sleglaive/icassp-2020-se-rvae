#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code implements the speech enhancement method published in: S. Leglaive, 
X. Alameda-Pineda, L. Girin, R. Horaud, "A recurrent variational autoencoder 
for speech enhancement", IEEE International Conference on Acoustics Speech and 
Signal Processing (ICASSP), Barcelona, Spain, 2020.

Copyright (c) 2019-2020 by Inria and CentraleSupelec
Authored by Simon Leglaive (simon.leglaive@centralesupelec.fr)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License (version 3) 
as published by the Free Software Foundation.

You should have received a copy of the GNU Affero General Public License
along with this program (LICENSE.txt). If not, see 
<https://www.gnu.org/licenses/>.
"""

my_seed = 0
import numpy as np
np.random.seed(my_seed)
import matplotlib.pyplot as plt
import librosa
import os
import pickle
import datetime
import pyloudnorm.pyloudnorm as pyln
import soundfile as sf
import json

# You have to choose between creating the train/val/test dataset

mode = 'train'
#mode = 'val'
#mode = 'test'

fs = 16000

# You have to change the paths below

if mode=='test':

    noise_types = ['CAFE-CAFE-2', 'HOME-LIVINGB-2', 'STREET-KG-2', 'CAR-WINUPB-2']
    
    SNRs = [-5, 0, 5]
    
    speech_dir = '/local_scratch/sileglai/datasets/clean_speech/wsj0_si_et_05'
    noise_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k'
    labels_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k/labels'
    
    speech_file_list = librosa.util.find_files(speech_dir, ext='wav')
    
    noise_file_list = librosa.util.find_files(noise_dir, ext='wav')
    # filter QUT-NOISE file list according to noise_types
    noise_file_list = [file for file in noise_file_list if 
                       len([noise for noise in noise_types if noise in file])!=0]
    
    json_file = 'QUT_WSJ0_test_dataset.json'
    
elif mode=='val':
    
    noise_types = ['CAFE-CAFE-1', 'HOME-LIVINGB-1', 'STREET-KG-1', 'CAR-WINUPB-1']
    
    SNRs = [-5, 0, 5]
    
    speech_dir = '/local_scratch/sileglai/datasets/clean_speech/wsj0_si_dt_05'
    noise_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k'
    labels_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k/labels'
    
    speech_file_list = librosa.util.find_files(speech_dir, ext='wav')
    
    noise_file_list = librosa.util.find_files(noise_dir, ext='wav')
    # filter QUT-NOISE file list according to noise_types
    noise_file_list = [file for file in noise_file_list if 
                       len([noise for noise in noise_types if noise in file])!=0]
    
    json_file = 'QUT_WSJ0_val_dataset.json'

elif mode=='train':
    
    noise_types = ['CAFE-FOODCOURTB-1', 'CAFE-FOODCOURTB-2', 
                   'HOME-KITCHEN-1','HOME-KITCHEN-2',
                   'STREET-CITY-1','STREET-CITY-2',
                   'CAR-WINDOWNB-1','CAR-WINDOWNB-2']
    
    SNRs = [-5, 0, 5]
    
    speech_dir = '/local_scratch/sileglai/datasets/clean_speech/wsj0_si_tr_s'
    noise_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k'
    labels_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k/labels'
    
    speech_file_list = librosa.util.find_files(speech_dir, ext='wav')
    
    noise_file_list = librosa.util.find_files(noise_dir, ext='wav')
    # filter QUT-NOISE file list according to noise_types
    noise_file_list = [file for file in noise_file_list if 
                       len([noise for noise in noise_types if noise in file])!=0]
    
    json_file = 'QUT_WSJ0_train_dataset.json'
    
    
    
dataset = []
    
for mix_num, speech_file in enumerate(speech_file_list):
    
    info_dict = {}
    
    # get speech file information
    path, utt_name = os.path.split(speech_file)
    utt_name = os.path.splitext(utt_name)[0]
    path, speaker = os.path.split(path)
    f = sf.SoundFile(speech_file)
    utt_len = len(f)
    
    info_dict['speaker'] = speaker
    info_dict['utt_name'] = utt_name
    info_dict['utt_len'] = utt_len

    # get transcripts
    prompt_file = os.path.join(path, 'prompts', speaker, utt_name[:-2] + '00.ptx')
    dot_file = os.path.join(path, 'dots', speaker, utt_name[:-2] + '00.dot')
    dot = ""
    prompt = ""
    with open(dot_file, "r") as f:
        for line in f:
            if utt_name in line:
                dot = line.split(' (')[0]
    with open(prompt_file, "r") as f:
        for line in f:
            if utt_name in line:
                prompt = line.split(' (')[0]
    if dot=="" or prompt=="":
        raise NameError('Prompt or dot not found')
        
    info_dict['dot'] = dot
    info_dict['prompt'] = prompt
    
    # uniformly draw a noise file and get information
    noise_file = noise_file_list[np.random.randint(0, len(noise_file_list))]
    _, noise_env = os.path.split(noise_file)
    noise_env = os.path.splitext(noise_env)[0]
    
    info_dict['noise_env'] = noise_env
    
    # get the boundaries (in seconds) of the main 30+ minute recording session
    label_file = os.path.join(labels_dir, noise_env + '.lab.txt')
    bound_beg = None
    bound_end = None
    with open(label_file, "r") as f:
        for line in f:
            if 'start' in line:
                bound_beg = np.int(np.float32(line.split()[0])*fs)
            if 'finish' in line:
                bound_end = np.int(np.float32(line.split()[0])*fs)
        
    # get noise segment beginning index
    noise_start = np.random.randint(bound_beg, bound_end-utt_len) 
    
    info_dict['noise_start'] = noise_start
    
    # uniformaly draw an SNR
    snr = SNRs[np.random.randint(0, len(SNRs))]
    
    info_dict['snr'] = snr
    
    dataset.append(info_dict)


with open(json_file, 'w') as outfile:  
    json.dump(dataset, outfile, indent=1)

#%%
    
plt.close('all')    

SNR_dataset = [info_dict['snr'] for info_dict in dataset]
env_dataset = [info_dict['noise_env'] for info_dict in dataset]

from collections import Counter

plt.figure()
env_counter = Counter(env_dataset)
frequencies = env_counter.values()
names = env_counter.keys()
x_coordinates = np.arange(len(env_counter))
plt.bar(x_coordinates, frequencies, align='center', tick_label=list(names))
plt.title('Histogram of noisy environments')

plt.figure()
snr_counter = Counter(SNR_dataset)
frequencies = snr_counter.values()
names = snr_counter.keys()
x_coordinates = np.arange(len(snr_counter))
plt.bar(x_coordinates, frequencies, align='center', tick_label=list(names))
plt.title('Histogram of SNRs')


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code implements the speech enhancement method published in: S. Leglaive, 
X. Alameda-Pineda, L. Girin, R. Horaud, "A recurrent variational autoencoder 
for speech enhancement", IEEE International Conference on Acoustics Speech and 
Signal Processing (ICASSP), Barcelona, Spain, 2020.

Copyright (c) 2019-2020 by Inria and CentraleSupelec
Authored by Simon Leglaive (simon.leglaive@centralesupelec.fr)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License (version 3) 
as published by the Free Software Foundation.

You should have received a copy of the GNU Affero General Public License
along with this program (LICENSE.txt). If not, see 
<https://www.gnu.org/licenses/>.
"""

my_seed = 0
import numpy as np
np.random.seed(my_seed)
import matplotlib.pyplot as plt
import librosa
import os
import pickle
import datetime
import pyloudnorm.pyloudnorm as pyln
import soundfile as sf
import random
from scipy.signal import resample_poly


#%%

# You have to change the paths below

noise_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE'
noise_file_list = librosa.util.find_files(noise_dir, ext='wav')

res_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k'

for noise_file in noise_file_list:

    path, noise_env = os.path.split(noise_file)
    noise_env = os.path.splitext(noise_env)[0]
    
    x, sr_orig = sf.read(noise_file)
    
    x_resampled = resample_poly(x, 16000, sr_orig)
    
    sf.write(os.path.join(res_dir, noise_env + '.wav'), x_resampled, 16000)

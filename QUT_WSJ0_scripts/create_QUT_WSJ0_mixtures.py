#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code implements the speech enhancement method published in: S. Leglaive, 
X. Alameda-Pineda, L. Girin, R. Horaud, "A recurrent variational autoencoder 
for speech enhancement", IEEE International Conference on Acoustics Speech and 
Signal Processing (ICASSP), Barcelona, Spain, 2020.

Copyright (c) 2019-2020 by Inria and CentraleSupelec
Authored by Simon Leglaive (simon.leglaive@centralesupelec.fr)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License (version 3) 
as published by the Free Software Foundation.

You should have received a copy of the GNU Affero General Public License
along with this program (LICENSE.txt). If not, see 
<https://www.gnu.org/licenses/>.
"""

my_seed = 0
import numpy as np
np.random.seed(my_seed)
import matplotlib.pyplot as plt
import librosa
import os
import pickle
import datetime
import pyloudnorm.pyloudnorm as pyln
import soundfile as sf
import json

#%% 

# You have to choose between creating the train/val/test dataset

mode = 'train'
#mode = 'val'
#mode = 'test'

fs = 16000

# You have to change the paths below

if mode=='test':
    
    speech_dir = '/local_scratch/sileglai/datasets/clean_speech/wsj0_si_et_05'
    noise_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k'
    mix_dir = '/local_scratch/sileglai/datasets/noisy_speech/single_channel/QUT_WSJ0/test'
    
    labels_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k/labels'
    
    json_file = 'QUT_WSJ0_test_dataset.json'
    
elif mode=='val':
    
    speech_dir = '/local_scratch/sileglai/datasets/clean_speech/wsj0_si_dt_05'
    noise_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k'
    mix_dir = '/local_scratch/sileglai/datasets/noisy_speech/single_channel/QUT_WSJ0/val'
    
    labels_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k/labels'
    
    json_file = 'QUT_WSJ0_val_dataset.json'
    
    
elif mode=='train':

    speech_dir = '/local_scratch/sileglai/datasets/clean_speech/wsj0_si_tr_s'
    noise_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k'
    mix_dir = '/local_scratch/sileglai/datasets/noisy_speech/single_channel/QUT_WSJ0/train'
    
    labels_dir = '/local_scratch/sileglai/datasets/QUT-NOISE/QUT-NOISE/QUT-NOISE-16k/labels'
    
    json_file = 'QUT_WSJ0_train_dataset.json'
    
with open(json_file) as f:  
    dataset = json.load(f)

fs = 16000
meter = pyln.Meter(fs) # create BS.1770 meter

for ind_mix, mix_info in enumerate(dataset):
    
    speaker = mix_info['speaker']
    utt_name = mix_info['utt_name']
    noise_env = mix_info['noise_env']
    noise_start = mix_info['noise_start']
    utt_len = mix_info['utt_len']
    target_snr = mix_info['snr']
    
    speech_file = os.path.join(speech_dir, speaker, utt_name + '.wav')
    noise_file = os.path.join(noise_dir, noise_env + '.wav')
    
    s, fs = sf.read(speech_file)  
    n, fs = sf.read(noise_file, start=noise_start, stop=noise_start+utt_len)
    n = n[:,0]
    
    s_loudness = meter.integrated_loudness(s)
    n_loudness = meter.integrated_loudness(n)
    
    input_snr = s_loudness - n_loudness
    scale_factor = 10**( (input_snr - target_snr)/20 )
    n = n*scale_factor
    x = s + n
    
    mix_file = os.path.join(mix_dir, utt_name + '_' + noise_env + '_' + str(target_snr) + '.wav')
    
    sf.write(mix_file, x, fs)
    
    print('file %d / %d' % (ind_mix+1, len(dataset)))
